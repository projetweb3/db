FROM mysql

RUN apt update && apt upgrade -y

COPY sql/ /docker-entrypoint-initdb.d/
